package linkogin

import (
	"errors"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

// PublicSaveFile date
func PublicSaveFile(nameForm string, ctx *gin.Context) (string, error) {
	file, err := ctx.FormFile(nameForm)
	if err != nil {
		return "", err
	}

	name := fmt.Sprintf("%d%d", rand.Int31(), time.Now().Nanosecond())
	SplitExt := strings.Split(file.Filename, ".")
	if len(SplitExt) == 2 {
		name += "save." + SplitExt[1]
	}
	if err := ctx.SaveUploadedFile(file, "public/"+name); err != nil {
		return "", err
	}

	return name, nil
}

// PublicRemoveFile in folder public
func PublicRemoveFile(name string) error {
	if !strings.Contains(name, "save.") {
		return errors.New("Not valid File")
	}
	if name != "" {
		return os.Remove("./public/" + name)
	}
	return errors.New("Not Name File")
}
