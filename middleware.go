package linkogin

import (
	"github.com/gin-gonic/gin"
)

// Middleware models
type Middleware struct {
}

// CacheControll header
func (Middleware) CacheControll(seconds string) func(*gin.Context) {
	return func(c *gin.Context) {
		c.Header("cache-control", "max-age="+seconds)
		c.Next()
	}
}
