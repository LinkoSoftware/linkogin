package linkogin

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// NewLinkCtr create newLinkocontroller
func NewLinkCtr(ctx *gin.Context) *LinkoController {
	var lkController = new(LinkoController)
	lkController.ginContext = ctx
	return lkController.init()
}

// LinkoController type
type LinkoController struct {
	ginContext    *gin.Context
	headerMessage []headerMessage
	exposeHeaders bool
}

type headerMessage struct {
	Code    string
	Message string
}

// MessageError Return Message Error
func (ctx *LinkoController) MessageError(a ...interface{}) *LinkoController {
	ctx.CustomMessage(fmt.Sprint(a...), ERROR)
	return ctx
}

// MessageWarning Return Message Warning
func (ctx *LinkoController) MessageWarning(a ...interface{}) *LinkoController {
	ctx.CustomMessage(fmt.Sprint(a...), WARNING)
	return ctx
}

// MessageSuccess Return Message Success
func (ctx *LinkoController) MessageSuccess(message ...interface{}) *LinkoController {
	ctx.CustomMessage(fmt.Sprint(message...), SUCCESS)
	return ctx
}

// Response complete
func (ctx *LinkoController) Response(code int, response interface{}, typeMessage string, message ...interface{}) *LinkoController {
	ctx.ginContext.JSON(code, response)
	ctx.CustomMessage(fmt.Sprint(message...), typeMessage)
	return ctx
}

// ResponseMessageError Return Message Error
func (ctx *LinkoController) ResponseMessageError(data interface{}, message ...interface{}) *LinkoController {
	ctx.ginContext.JSON(200, data)
	ctx.CustomMessage(fmt.Sprint(message...), ERROR)
	return ctx
}

// ResponseMessageWarning Return Message Warning
func (ctx *LinkoController) ResponseMessageWarning(data interface{}, message ...interface{}) *LinkoController {
	ctx.ginContext.JSON(200, data)
	ctx.CustomMessage(fmt.Sprint(message...), WARNING)
	return ctx
}

// ResponseMessageSuccess Return Message Success
func (ctx *LinkoController) ResponseMessageSuccess(data interface{}, message ...interface{}) *LinkoController {
	ctx.ginContext.JSON(200, data)
	ctx.CustomMessage(fmt.Sprint(message...), SUCCESS)
	return ctx
}

// CustomMessage add Message in header
func (ctx *LinkoController) CustomMessage(message string, code string) {
	if ctx.exposeHeaders == false {
		ctx.exposeHeaders = true
		ctx.ginContext.Header("access-control-expose-headers", "message-code, message-text")
	}
	var meesageHeader string
	var codeHeader string
	for _, val := range ctx.headerMessage {
		meesageHeader += val.Message + ";"
		codeHeader += val.Code + ";"
	}
	ctx.headerMessage = append(ctx.headerMessage, headerMessage{Code: code, Message: message})
	ctx.ginContext.Header("message-code", codeHeader+code)
	ctx.ginContext.Header("message-text", meesageHeader+message)
}

// Code response
func (ctx *LinkoController) Code(code int) {
	ctx.ginContext.Status(code)
}

// Bind is a shortcut for c.MustBindWith(obj, binding.JSON).
func (ctx *LinkoController) Bind(data interface{}) bool {
	return ctx.ginContext.BindJSON(data) == nil
}

func (ctx *LinkoController) init() *LinkoController {
	// View Need Cache
	if cacherTime := ctx.ginContext.GetHeader("cache-time"); cacherTime != "" {
		ctx.ginContext.Header("cache-control", "max-age="+cacherTime)
	}
	return ctx
}

// R response
func R(ctx *gin.Context, code int, response interface{}, typeMessage string, message ...interface{}) {
	NewLinkCtr(ctx).Response(code, response, typeMessage, message...)
}
